
__title__ = 'opsparkctl'
__description__ = 'Spark on Kubernetes and Openstack cluster management tool'
__url__ = 'https://github.com/cerndb/spark-on-k8s-operator/tree/master/opsparkctl'
__version__ = '0.1.0'
__build__ = 0x021804
__author__ = 'CERN-IT-DB-SAS'
__author_email__ = 'piotr.mrowczynski@cern.ch'
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2018 CERN'
__python_requires__ = '>=2.7.5, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*'
__python_min_version__ = '2.7.5'
