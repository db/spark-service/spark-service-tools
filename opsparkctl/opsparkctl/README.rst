Spark on Kubernetes and Openstack management tool
#################################################

**This work is in progress, experimental and for evaluation purposes only**

Command line tool `opsparkctl` holds all tools for creating/deleting/resizing/reinitializing spark-on-k8s cluster on Openstack, based on `spark-on-k8s-operator`.

Idea is to provide users and admins an architecture, in which user in general should be able to fetch configuration of the cluster he is allowed to use, and be able to seemlessly submit Spark jobs using `sparkctl`.

`Usage Manual: Spark on Kubernetes and Openstack management tool <https://github.com/cerndb/spark-on-k8s-operator/blob/master/opsparkctl>`_


