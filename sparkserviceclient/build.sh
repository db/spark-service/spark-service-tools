#!/usr/bin/env bash

sudo rm -rf dist build cern_spark_service.egg-info

python setup.py sdist bdist_wheel

sudo python setup.py install