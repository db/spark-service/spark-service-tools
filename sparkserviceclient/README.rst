CERN Spark Service Tools
########################################

**Work In Progress - Experimental**

CERN Spark Service package holds all tools for creating/deleting/resizing/reinitializing Spark on Kubernetes cluster,
and running spark-submit job natively using Python PIP package without having to install any Spark configurations and dependencies - spark-submit
runs inside the cluster

Package is installed with ``pip``. Examples of package usage can be found in `CERN Spark EOS Examples <https://gitlab.cern.ch/db/spark-service/spark-service-examples>`_

After spark-on-kubernetes cluster creation run demo spark job:

.. code-block::

    $ cern-spark-service spark-test \
    --cluster <cluster-name>

Run full example:

.. code-block::

    $ cern-spark-service spark-submit \
    --cluster spark-cluster \
    --conf spark.executor.instances=20 \
    --class org.sparkservice.sparkrootapplications.examples.DimuonReductionAOD \
    --jars http://central.maven.org/maven2/org/diana-hep/spark-root_2.11/0.1.16/spark-root_2.11-0.1.16.jar,http://central.maven.org/maven2/org/diana-hep/histogrammar-sparksql_2.11/1.0.3/histogrammar-sparksql_2.11-1.0.3.jar,http://central.maven.org/maven2/org/diana-hep/root4j/0.1.6/root4j-0.1.6.jar,http://central.maven.org/maven2/org/diana-hep/histogrammar_2.11/1.0.3/histogrammar_2.11-1.0.3.jar,http://central.maven.org/maven2/org/apache/bcel/bcel/5.2/bcel-5.2.jar,http://central.maven.org/maven2/org/tukaani/xz/1.2/xz-1.2.jar,http://central.maven.org/maven2/jakarta-regexp/jakarta-regexp/1.4/jakarta-regexp-1.4.jar \
    <path-to-examples>/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar \
    root://eospublic.cern.ch//eos/opendata/cms/MonteCarlo2012/Summer12_DR53X/DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball/AODSIM/PU_RD1_START53_V7N-v1/20000/DCF94DC3-42CE-E211-867A-001E67398011.root

Table of Contents
=================

.. contents:: `Table of Contents`_

Architecture overview
=====================
Idea is to provide users and admin an architecture, in which user in general should be able to fetch configuration of the cluster he is allowed to use, and be able to seemlessly submit Spark jobs.

After Kubernetes cluster is created, additional self-healing services will be initialized inside the cluster - Spark Resource Staging Server, Spark Shuffle Service, Spark Checkpoint Storage and Spark Edge (see `Spark-on-Kubernetes dependencies defaults`_).

**Spark Edge Pod** - spark edge is used to run spark-submit binary, thus allowing cern-spark-service python package user not have spark dependencies installed on local computer and not to be aware of kubernetes cluster configuration.

**Spark Resource Staging Server** - application dependencies that are being used in submitted job need to be sent to a resource staging server that the driver and executor can then communicate with to retrieve those dependencies.

**Spark Shuffle Service** - to support Dynamic Allocation with cluster mode, running a shuffle service is required.

**Spark Checkpoints Storage for Spark Streaming** - to enable checkpointing in the Spark Streaming app, running a checkpoint storage and shuffle service is required.

Future work
===========

* Using custom kubernetes cluster template (see `Default kubernetes template over Openstack`_)

* Custom spark cluster configuration, including spark-defaults.conf (see `Spark-on-Kubernetes dependencies defaults`_)

* Fetching currently used kubernetes cluster template

* Deleting/resizing the kubernetes cluster over Openstack

* Allow shuffle service

* Allow streaming

Prerequisites
=============

* At least Python 2.7.5 (with yum install python-pip python-devel krb5-devel)

* `Python PIP <https://pip.pypa.io/en/stable/installing/>`_

* Recommended CENTOS 7, Ubuntu, MacOS

Installation
============

On MacOS and Linux, tools can be installed via `Python PIP <https://pip.pypa.io/en/stable/installing/>`_

* In case you have sudo access

.. code-block:: bash

    $ pip install --upgrade cern-spark-service
    # or
    $ sudo -H pip install --upgrade cern-spark-service
    # test with
    $ cern-spark-service --help

* In case you are on environment that does not have sudo access (lxplus-cloud, lxplus7). To avoid using local bin directly, perform additional step `Map .local/bin folder to use directly`_. If you experience some errors installing try `Errors during pip install`_

.. code-block:: bash

    $ pip install --user --upgrade cern-spark-service
    # test with
    $ $HOME/.local/bin/cern-spark-service --help

Usage
=====

Create
''''''

**Create new kubernetes cluster in default namespace**

* To create a spark-on-kubernetes cluster over Openstack, initialize spark service and fetch configuration locally. Command will guide you through creation process

.. code-block::

    $ cern-spark-service create --openstack


**With existing kubernetes cluster in default namespace**


* If you already have a kubernetes cluster created, and configuration stored locally (for more info `Local cluster configuration example`_), you can just create/recreate spark-on-kubernetes cluster dependencies. Command will guide you through the process

.. code-block::

    $ cern-spark-service create --only-spark

**After creation, run spark-test command in order to check your cluster**

Fetch
'''''

* To fetch locally kubernetes cluster configuration from Openstack and initialize your local configuration (for more info `Local cluster configuration example`_). Command will guide you through creation process

.. code-block::

    $ cern-spark-service fetch --openstack-config

* To display kubernetes cluster info from Openstack. Command will guide you through creation process

.. code-block::

    $ cern-spark-service fetch --openstack-cluster-info

Spark Test
''''''''''

* To perform quick check of status of spark on kubernetes cluster, use quick check

.. code-block::

    $ cern-spark-service spark-test --cluster <your-cluster-name> --quick-check

* To run exemplary test spark-submit job, which will request as many executors as there are nodes. This task can take long since it forces each kubernetes minion to update their images.

.. code-block::

    $ cern-spark-service spark-test --cluster <your-cluster-name>

Spark Submit
''''''''''''

* To run Spark Submit over Spark on Kubernetes.

.. code-block::

    $ cern-spark-service spark-submit \
    --cluster <your-cluster-name> \
    --class <your-spark-job-class-name> \
    --conf <your-custom-conf> \
    --jars http://<your-dependency-1>,http://<your-dependency-1> \
    <path-to-examples>/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar

* Check also `Spark (EOS, ROOT) application examples <https://gitlab.cern.ch/db/spark-service/spark-service-examples>`_. Example of such application is

.. code-block::

    $ cern-spark-service spark-submit \
    --cluster spark-cluster \
    --class org.sparkservice.sparkrootapplications.examples.SparkPi \
    <path-to-examples>/spark-service/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar

or

.. code-block::

    $ cern-spark-service spark-submit \
    --cluster spark-cluster \
    --conf spark.executor.instances=20 \
    --class org.sparkservice.sparkrootapplications.examples.DimuonReductionAOD \
    --jars http://central.maven.org/maven2/org/diana-hep/spark-root_2.11/0.1.16/spark-root_2.11-0.1.16.jar,http://central.maven.org/maven2/org/diana-hep/histogrammar-sparksql_2.11/1.0.3/histogrammar-sparksql_2.11-1.0.3.jar,http://central.maven.org/maven2/org/diana-hep/root4j/0.1.6/root4j-0.1.6.jar,http://central.maven.org/maven2/org/diana-hep/histogrammar_2.11/1.0.3/histogrammar_2.11-1.0.3.jar,http://central.maven.org/maven2/org/apache/bcel/bcel/5.2/bcel-5.2.jar,http://central.maven.org/maven2/org/tukaani/xz/1.2/xz-1.2.jar,http://central.maven.org/maven2/jakarta-regexp/jakarta-regexp/1.4/jakarta-regexp-1.4.jar \
    <path-to-examples>/spark-service/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar \
    root://eospublic.cern.ch//eos/opendata/cms/MonteCarlo2012/Summer12_DR53X/DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball/AODSIM/PU_RD1_START53_V7N-v1/20000/DCF94DC3-42CE-E211-867A-001E67398011.root

Spark Submit - Secure EOS
''''''''''''''''''''''''''

* To run Spark Submit over Spark on Kubernetes and secure EOS.

.. code-block::

    $ cern-spark-service spark-submit \
    --cluster <your-cluster-name> \
    --class <your-spark-job-class-name> \
    --conf <your-custom-conf> \
    --jars http://<your-dependency-1>,http://<your-dependency-2> \
    --eos-auth <path-to-krb5-ticket> \
    <path-to-examples>/spark-service/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar \
    root://<instance-name>/<path-to-file>

* Example of such application would be:

.. code-block::

    $ export USER=<your-user>
    $ kinit -c /tmp/krb5cc_$USER $USER
    $ cern-spark-service spark-submit \
    --cluster spark-cluster \
    --class org.sparkservice.sparkrootapplications.examples.EventsSelect \
    --eos-auth /tmp/krb5cc_$USER \
    --jars \
    http://central.maven.org/maven2/org/diana-hep/root4j/0.1.6/root4j-0.1.6.jar,\
    http://central.maven.org/maven2/org/apache/bcel/bcel/5.2/bcel-5.2.jar,\
    http://central.maven.org/maven2/org/diana-hep/spark-root_2.11/0.1.16/spark-root_2.11-0.1.16.jar \
    path-to-examples>/spark-service/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar \
    root://eosuser.cern.ch/eos/user/o/opendata/evsel.root

Spark Submit - Secure HDFS
''''''''''''''''''''''''''

* To run Spark Submit over Spark on Kubernetes and secure HDFS.

.. code-block::

    $ cern-spark-service spark-submit \
    --cluster <your-cluster-name> \
    --class <your-spark-job-class-name> \
    --conf <your-custom-conf> \
    --jars http://<your-dependency-1>,http://<your-dependency-2> \
    --hdfs-auth <path-to-krb5-ticket> \
    --hdfs-config-files http://<your-config-1>,http://<your-config-2> \
    <path-to-examples>/spark-service/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar \
    hdfs://<cluster-name>/<path-to-file>

* Example of such application would be:

.. code-block::

    $ export USER=<your-user>
    $ kinit -c /tmp/krb5cc_$USER $USER
    $ cern-spark-service spark-submit \
    --cluster spark-cluster \
    --class org.sparkservice.sparkrootapplications.examples.EventsSelect \
    --hdfs-auth /tmp/krb5cc_$USER \
    --hdfs-config-files \
    https://hadoop-config.web.cern.ch/files/hadoop/conf/etc/hadalytic/hadoop.hadalytic/hdfs-site.xml,\
    https://hadoop-config.web.cern.ch/files/hadoop/conf/etc/hadalytic/hadoop.hadalytic/topology.table.file,\
    https://hadoop-config.web.cern.ch/files/hadoop/conf/etc/hadalytic/hadoop.hadalytic/core-site.xml \
    --jars \
    http://central.maven.org/maven2/org/diana-hep/root4j/0.1.6/root4j-0.1.6.jar,\
    http://central.maven.org/maven2/org/apache/bcel/bcel/5.2/bcel-5.2.jar,\
    http://central.maven.org/maven2/org/diana-hep/spark-root_2.11/0.1.16/spark-root_2.11-0.1.16.jar \
    path-to-examples>/spark-service/spark-service-examples/target/scala-2.11/spark-service-examples_2.11-0.0.1.jar \
    hdfs://hadalytic/user/opendata/evsel.root

**Note:** Package will handle all the authentication, configuration, logging and job handling for the user. Command ``spark-submit`` will be executed in a dedicated container inside the instance

**Note:** User must specify ``--cluster <cluster-name>`` instead of ``--master``

**Note:** User must specify  ``--jars http://<your-dependency-1>,http://<your-dependency-1>`` instead of ``--packages``, thus is required to specify full urls of the required dependencies

**Note:** With existing configuration for you cluster ``<your-cluster-name>`` at ``~/.cern-spark-service/<your-cluster-name>/`` you can run spark-submit command, with all the configurations as on normal Apache Spark cluster.

Default spark configuration
===========================

Local cluster configuration example
'''''''''''''''''''''''''''''''''''

Typical local cluster configuration is stored at ``.cern-spark-service/<your-cluster-name>/`` and has following contents

.. code-block::

    $ ls .cern-spark-service/<your-cluster-name>/
    ca.pem cert.pem config key.pem

Configuration file ``config`` has following contents:

.. code-block::

    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority: <path-to-user-home>/.cern-spark-service/<your-cluster-name>/ca.pem
        server: https://<kube-master-ip>:6443
      name: <your-cluster-name>
    contexts:
    - context:
        cluster: <your-cluster-name>
        user: admin
      name: default
    current-context: default
    kind: Config
    preferences: {}
    users:
    - name: admin
      user:
        client-certificate: <path-to-user-home>/.cern-spark-service/<your-cluster-name>/cert.pem
        client-key: <path-to-user-home>/.cern-spark-service/<your-cluster-name>/key.pem

Default kubernetes template over Openstack
''''''''''''''''''''''''''''''''''''''''''

Currently default ``kubernetes`` template of Openstack is used.

To get details, please visit `https://openstack.cern.ch <https://openstack.cern.ch>`_, navigate to ``<your-project-name>`` --> ``Container Infra`` --> ``Cluster templates``

Spark-on-Kubernetes dependencies defaults
'''''''''''''''''''''''''''''''''''''''''

Currently command supports only default spark-kubernetes configuration, with ``spark-driver`` and ``spark-executor`` docker images being stored at ``gitlab-registry.cern.ch/db/spark-service/docker-registry``

Default ``resource-staging-server.conf`` and ``spark-default.conf``:

.. code-block::

    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: spark-config
    data:
      rss-defaults.conf: |
        spark.kubernetes.resourceStagingServer.port=10000
        spark.ssl.kubernetes.resourceStagingServer.enabled=false
        spark.kubernetes.authenticate.resourceStagingServer.useServiceAccountCredentials=false
        spark.kubernetes.authenticate.resourceStagingServer.caCertFile=/spark-secrets/client-ca
        spark.kubernetes.authenticate.resourceStagingServer.clientKeyFile=/spark-secrets/client-key
        spark.kubernetes.authenticate.resourceStagingServer.clientCertFile=/spark-secrets/client-cert
      spark-defaults.conf: |
        spark.submit.deployMode=cluster
        spark.kubernetes.docker.image.pullPolicy=Always
        spark.kubernetes.authenticate.submission.caCertFile=/spark-secrets/client-ca
        spark.kubernetes.authenticate.submission.clientKeyFile=/spark-secrets/client-key
        spark.kubernetes.authenticate.submission.clientCertFile=/spark-secrets/client-cert
        spark.kubernetes.authenticate.driver.caCertFile=/spark-secrets/client-ca
        spark.kubernetes.authenticate.driver.clientKeyFile=/spark-secrets/client-key
        spark.kubernetes.authenticate.driver.clientCertFile=/spark-secrets/client-cert
        spark.kubernetes.driver.docker.image=gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-driver
        spark.kubernetes.executor.docker.image=gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-executor
        spark.kubernetes.initcontainer.docker.image=gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-init

Resource staging server template

.. code-block::

    apiVersion: apps/v1beta1
    kind: Deployment
    metadata:
      name: spark-resource-staging-server
    spec:
      replicas: 1
      template:
        metadata:
          labels:
            resource-staging-server-instance: default
        spec:
          volumes:
            - name: resource-staging-server-properties
              configMap:
                name: spark-config
            - name: client-certs
              secret:
                secretName: spark-secrets
          containers:
            - name: spark-resource-staging-server
              image: gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-resource-staging-server
              resources:
                requests:
                  cpu: 100m
                  memory: 256Mi
                limits:
                  cpu: 100m
                  memory: 1Gi
              volumeMounts:
                - name: resource-staging-server-properties
                  mountPath: '/etc/spark-resource-staging-server'
                - name: client-certs
                  mountPath: '/spark-secrets'
              args:
                - '/etc/spark-resource-staging-server/rss-defaults.conf'

    apiVersion: v1
    kind: Service
    metadata:
      name: spark-resource-staging-service
    spec:
      type: NodePort
      selector:
        resource-staging-server-instance: default
      ports:
        - protocol: TCP
          port: 10000
          targetPort: 10000
          nodePort: 31000

Spark edge node template

.. code-block::

    apiVersion: apps/v1beta1
    kind: Deployment
    metadata:
      name: spark-edge
    spec:
      replicas: 1
      template:
        metadata:
          labels:
            spark-edge-instance: default
        spec:
          volumes:
            - name: spark-config-properties
              configMap:
                name: spark-config
            - name: client-certs
              secret:
                secretName: spark-secrets
          containers:
            - name: spark-edge
              image: gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-edge
              resources:
                requests:
                  cpu: 1000m
                  memory: 2Gi
                limits:
                  cpu: 2000m
                  memory: 4Gi
              volumeMounts:
                - name: spark-config-properties
                  mountPath: '/opt/spark/conf'
                - name: client-certs
                  mountPath: '/spark-secrets'

Cluster secrets used in resource-staging-server and spark-edge containers internally in the cluster

.. code-block::

    apiVersion: v1
    kind: Secret
    type: Opaque
    metadata:
      name: spark-secrets
    data:
      # Populate the following files with etcd TLS configuration if desired, but leave blank if
      # not using TLS for etcd.
      # This self-hosted install expects three files with the following names.  The values
      # should be base64 encoded strings of the entire contents of each file.
      client-key: {key}
      client-cert: {cert}
      client-ca: {ca}

Recommendations
===============

Map .local/bin folder to use directly
'''''''''''''''''''''''''''''''''''''

To add ``$HOME/.local/bin`` so you can use ``cern-spark-service``
directly, edit your ``bashrc_profile`` file using e.g. ``nano`` or
``vim``

.. code-block::

    $ nano ~/.bash_profile

Add this to the end of the file

.. code-block::

    $ PATH=$PATH:$HOME/.local/bin
    $ export PATH

Logout or use

.. code-block::

    $ source ~/.bash_profile

Test with e.g.

.. code-block::

    $ cern-spark-service --help

Errors during pip install
'''''''''''''''''''''''''

In case you are receiving errors during pip installation, please retry with cleaning some pip local files, and ensure
that you don't have some custom python paths temporarly enabled

To clear local packages on Linux

.. code-block::

    $ rm -rf $HOME/.local/lib/python2.7
    $ rm -rf $HOME/.cache/pip

To check what python version is used

.. code-block::

    $ python --version

To check what python is used

.. code-block::

    $ which python

Known CentOS/RHEL/lxplus-cloud issue
''''''''''''''''''''''''''''''''''''

There is known ``python-kubernetes`` issue due to old python version
(2.7.5) in CENTOS. Thus on CENTOS/RHEL/LXPLUS-CLOUD use pip local user
install

To enable newest python version using RedHat Software Collection (scl)
and add ``$HOME/.local/bin`` so you can use ``cern-spark-service``
directly, edit your ``bashrc_profile`` file using e.g. ``nano`` or
``vim``

.. code-block::

    $ nano ~/.bash_profile

Add this to the end of the file

.. code-block::

    $ exec scl enable python27 -- bash "$@"

Logout or use

.. code-block::

    $ source ~/.bash_profile

Use pip local user install. It installs cern-spark-service in
``$HOME/.local/bin/cern-spark-service``.

.. code-block::

    $ pip install --user --upgrade cern-spark-service

Publishing
==========

Increase the version and correct documentation in

.. code-block::

    $ cernsparkservice/__version__.py

Cleanup

.. code-block::

    $ rm -rf build cern_spark_service.egg-info dist

Publish

.. code-block::

    $ python setup.py publish

