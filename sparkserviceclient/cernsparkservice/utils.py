import base64
import datetime
import os
import tarfile
import urllib


def read_cert(certificate_path):
    with open(certificate_path) as file:
        return base64.encodestring(file.read()).replace('\n', '')


def get_timestamp():
    return int(datetime.datetime.now().strftime("%s")) * 1000


def get_tar_files(url):
    file_handler = urllib.urlretrieve(url, filename=None)[0]
    base_name = os.path.basename(url)
    tar = tarfile.open(file_handler)

    tared_files = []
    for member in tar.getmembers():
        f=tar.extractfile(member)
        #f.read()
        #f.name
        tared_files.append(f)
    tar.close()

    return tared_files