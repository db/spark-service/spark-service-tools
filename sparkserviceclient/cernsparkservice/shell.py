"""Command-line interface to the CERN Spark on Kubernetes"""

import argparse
import sys
from . import __version__

min_version = __version__.__python_min_version__.split('.')
if not (sys.version_info[0] >= int(min_version[0]) and sys.version_info[1] >= int(min_version[1]) and sys.version_info[2] >= int(min_version[2])):
    print "Your python version(%s) is lower than required (%s)" % (
    str(sys.version_info[:3]), __version__.__python_min_version__)
    print "Please refer to documentation for help"
    print "%s" % (__version__.__url__)
    exit(2)

from . import kube_create_command
from . import kube_fetch_command
from . import spark_submit_command
from . import spark_test_command

class SparkServiceArgumentParser(argparse.ArgumentParser):

    def __init__(self, *args, **kwargs):
        super(SparkServiceArgumentParser, self).__init__(*args, **kwargs)

    def error(self, message):
        """error(message: string)
        Prints a usage message incorporating the message to stderr and
        exits.
        """
        self.print_usage(sys.stderr)
        choose_from = ' (choose from'
        progparts = self.prog.partition(' ')
        self.exit(2, "error: %(errmsg)s\nTry '%(mainp)s --help %(subp)s'"
                     " for more information.\n" %
                     {'errmsg': message.split(choose_from)[0],
                      'mainp': progparts[0],
                      'subp': progparts[2]})

class SparkServiceShell():
    def run(self, argv):
        parser = SparkServiceArgumentParser(
            prog='cern-spark-service',
            description=__doc__.strip(),
            epilog='See "cern-spark-service help COMMAND" '
                   'for help on a specific command.',
            add_help=True,
        )

        parser.add_argument('-v', '--version', action='version', version="%(prog)s (" + __version__.__version__ + ")")

        subparsers = parser.add_subparsers(help='commands', dest='command')

        # Create command
        create_parser = subparsers.add_parser(
            'create', help='Create, configure and initialize spark on kubernetes cluster')
        create_parser.add_argument(
            '--openstack', default=False, action='store_true', dest='openstack',
            help='Create a kubernetes cluster over Openstack, initialize spark service and fetch configuration locally',
        )
        create_parser.add_argument(
            '--only-spark', default=False, action='store_true', dest='only_spark',
            help='Recreate spark service from local configuration of existing kubernetes cluster',
        )

        # Fetch command
        fetch_parser = subparsers.add_parser(
            'fetch', help='Fetch configuration of spark on kubernetes cluster')
        fetch_parser.add_argument("--openstack-config", default=False, action='store_true', dest='openstack_config', help='Select cluster and fetch locally kubernetes cluster configuration from Openstack')
        fetch_parser.add_argument("--openstack-cluster-info", default=False, action='store_true', dest='openstack_cluster_info', help='Select cluster and fetch kubernetes cluster info from Openstack')

        # Test command
        spark_test_parser = subparsers.add_parser(
            'spark-test', help='Runs test spark-submit job, which will request as many executors as there are nodes. This task can take long since it forces each kubernetes minion to update their images.')
        spark_test_parser.add_argument("--cluster", type=str, help='The name of the kubernetes cluster', required=True)
        spark_test_parser.add_argument("--quick-check", default=False, action='store_true', dest='quick_check', help='Perform quick test, which will just fetch information about spark service long running dependencies')

        # Spark-Submit command
        spark_submit_parser = subparsers.add_parser(
            'spark-submit', help='Submit a spark job')
        spark_submit_parser.add_argument("--cluster", type=str, help='The name of the kubernetes cluster', required=True)
        spark_submit_parser.add_argument("--hdfs-config-files", type=str, help='The url pointing to location of hadoop config files', dest='hdfs_config_files')
        spark_submit_parser.add_argument("--hdfs-auth", type=str, help='The location of valid Kerberos ticket used for authentication of hdfs storage', dest='hdfs_auth')
        spark_submit_parser.add_argument("--eos-auth", type=str, help='The location of valid Kerberos ticket used for authentication of eos storage', dest='eos_auth')

        # Parse and route to proper class
        args, additional = parser.parse_known_args()
        if args.command == 'create':
            if not kube_create_command.KubeCreateCommand().run(args, additional):
                print "** ERROR **"
                print "Check command, for help: cern-spark-service create --help "
        elif args.command == 'spark-submit':
            if not spark_submit_command.SparkSubmitCommand().run(args, additional):
                print "** ERROR **"
                print "Check command, cluster does not exists or wrong configuration, check with: cern-spark-service spark-test --help "
        elif args.command == 'fetch':
            if not kube_fetch_command.KubeFetchCommand().run(args, additional):
                print "** ERROR **"
                print "Check command, for help: cern-spark-service fetch --help "
        elif args.command == 'spark-test':
            if not spark_test_command.SparkTestCommand().run(args, additional):
                print "** ERROR **"
                print "Check command, for help: cern-spark-service spark-test --help "
def main(argv=None):
    return SparkServiceShell().run(argv)

if __name__ == "__main__":
    sys.exit(main())