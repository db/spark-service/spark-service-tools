from . import base_command
from . import cluster
from . import kube_config_client

class SparkTestCommand(base_command.BaseCommand):

    def run(self, args, additional):
        """
        :param args array: arguments passed to the command via parser
        :param args array: arguments passed to the command which are custom options
        :return: bool: whether command was successful or not
        """

        config = kube_config_client.KubeConfigClient()

        # Load the client configuration
        cluster_name = args.cluster
        configuration = None
        try:
            configuration = config.load_local_config(cluster_name)
        except Exception as e:
            print e.message

        # If configured, do check
        if configuration:
            spark_kube_api = cluster.SparkKubeApi(configuration)

            # Check dependecies
            spark_kube_api.check_spark_deps()

            # Get all nodes
            nodes = spark_kube_api.get_labeled_nodes(label_selector=None)

            if args.quick_check:
                return True

            executors = len(nodes)
            tasks = executors * 1000
            conf_args_list = ["--conf", "spark.executor.instances=%s" % executors, "--class", "org.apache.spark.examples.SparkPi"]
            jar_args = ["local:///opt/spark/examples/jars/spark-examples_2.11-2.2.0-k8s-0.5.0.jar", "%s" % tasks]
            job_file_paths = []
            spark_kube_api.exec_spark_submit(
                conf_args_list=conf_args_list,
                job_file_paths=job_file_paths,
                source_jar_path=jar_args[0],
                jar_args=jar_args[1:]
            )

            return True

        return False
