
class BaseCommand(object):
    def run(self, args, additional):
        raise Exception('This Command is not supported')