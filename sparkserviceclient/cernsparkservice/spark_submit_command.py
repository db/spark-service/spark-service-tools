from . import base_command
from . import cluster
from . import kube_config_client

class SparkSubmitCommand(base_command.BaseCommand):

    def run(self, args, additional):
        """
        :param args array: arguments passed to the command via parser
        :param args array: arguments passed to the command which are custom options
        :return: bool: whether command was successful or not
        """

        config = kube_config_client.KubeConfigClient()

        # Load the client configuration
        cluster_name = args.cluster
        configuration = None
        try:
            configuration = config.load_local_config(cluster_name)
        except Exception as e:
            print e.message

        # Parse spark-submit - separate spark options from jar path and jar args
        is_next_conf_arg = False
        is_next_files_arg = False
        conf_args_list = []
        job_file_paths = []
        jar_args = []
        for additional_arg in additional:
            if is_next_conf_arg:
                conf_args_list.append(additional_arg)
                is_next_conf_arg = False
            elif is_next_files_arg:
                job_file_paths = additional_arg.split(",")
                is_next_files_arg = False
            elif additional_arg.startswith('--files'):
                is_next_files_arg = True
            elif additional_arg.startswith('--'):
                conf_args_list.append(additional_arg)
                is_next_conf_arg = True
            else:
                jar_args.append(additional_arg)
        if len(jar_args) < 1:
            return False

        hdfs_config_files_url_list = []
        if args.hdfs_config_files:
            hdfs_config_files_url_list = args.hdfs_config_files.split(",")

        # If configured, do check
        if configuration:
            spark_kube_api = cluster.SparkKubeApi(configuration)

            spark_kube_api.exec_spark_submit(
                conf_args_list=conf_args_list,
                job_file_paths=job_file_paths,
                source_jar_path=jar_args[0],
                jar_args=jar_args[1:],
                hdfs_auth=args.hdfs_auth,
                eos_auth=args.eos_auth,
                hdfs_config_files=hdfs_config_files_url_list
            )

            return True

        return False
