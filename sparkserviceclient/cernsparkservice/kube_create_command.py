from . import base_command
from . import cluster
from . import kube_config_client
from . import kube_fetch_command

class SparkKubeCreate():
    @staticmethod
    def create_spark_on_cluster(kube_cluster_name):
        print
        print "Init Spark on Kubernetes Resource Staging Server and Spark Submit Pod.."

        # Initialize spark kube api from local config (if exists)
        client = kube_config_client.KubeConfigClient()
        configuration = client.load_local_config(kube_cluster_name)
        spark_kube_api = cluster.SparkKubeApi(configuration)

        # Create Spark Resource Staging Server and Spark Submit pod in kubernetes cluster
        spark_kube_api.create_spark_deps()

        print
        print "Finished spark on kubernetes creation"
        return True

class KubeCreateCommand(base_command.BaseCommand):
    def run(self, args, additional):
        """
        :param args array: arguments passed to the command
        :return: bool: whether command was successful or not
        """

        if args.openstack:
            return OpenstackKubeCreateCommand().run()
        elif args.only_spark:
            return LocalKubeCreateCommand().run()
        else:
            return False

class OpenstackKubeCreateCommand():
    def run(self):
        """
        :param skip_cluster bool: indicates whether the cluster should be created, if false only cluster configuration will be fetched
        :return: KubeCluster
        """
        from . import openstack_client
        spark_kube_create = SparkKubeCreate()
        kube_fetch = kube_fetch_command.KubeFetch()

        # Initialize client
        client = openstack_client.CernOpentackClient()

        # Select a project in openstack
        selected_project = client.get_project()

        # Create kube cluster
        name = client.create_cluster(selected_project.id)

        # Get created cluster config
        kube_cluster_info = client.get_cluster(selected_project.id, name)

        # Create/refresh local config
        kube_fetch.save_config(kube_cluster_info)

        # Create/refresh spark dependecies on the cluster
        spark_kube_create.create_spark_on_cluster(name)

        return True

class LocalKubeCreateCommand():
    def run(self):
        """
        :return: KubeCluster
        """
        spark_kube_create = SparkKubeCreate()
        client = kube_config_client.KubeConfigClient()

        cluster_name = client.find_cluster_config()

        spark_kube_create.create_spark_on_cluster(cluster_name)

        return True
