import qprompt
from kubernetes import config
from kubernetes.client import Configuration
import os
import errno

class KubeConfigClient(object):
    def __init__(self):
        self._cfg_dir_base = os.path.join(os.path.expanduser("~"), '.cern-spark-service')
        self._prepare_cfg_dir(self._cfg_dir_base)

    def _prepare_cfg_dir(self, path):
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

    def save_cluster_info(self, kube_cluster_info):
        self._cfg_dir = os.path.join(self._cfg_dir_base, kube_cluster_info.name)
        self._cfg_file = os.path.join(self._cfg_dir, 'config')
        self._prepare_cfg_dir(self._cfg_dir)

        for k in ('key', 'cert', 'ca'):
            attribute = getattr(kube_cluster_info, "%s" % k)

            fname = "%s/%s.pem" % (self._cfg_dir, k)
            f = open(fname, "w")
            f.write(attribute)
            f.close()

        cfg = ("apiVersion: v1\n"
               "clusters:\n"
               "- cluster:\n"
               "    certificate-authority: %(cfg_dir)s/ca.pem\n"
               "    server: %(api_address)s\n"
               "  name: %(name)s\n"
               "contexts:\n"
               "- context:\n"
               "    cluster: %(name)s\n"
               "    user: admin\n"
               "  name: default\n"
               "current-context: default\n"
               "kind: Config\n"
               "preferences: {}\n"
               "users:\n"
               "- name: admin\n"
               "  user:\n"
               "    client-certificate: %(cfg_dir)s/cert.pem\n"
               "    client-key: %(cfg_dir)s/key.pem\n"
               % {'name': kube_cluster_info.name,
                  'api_address': kube_cluster_info.api_address,
                  'cfg_dir': self._cfg_dir})

        f = open(self._cfg_file, "w")
        f.write(cfg)
        f.close()

    def load_local_config(self, name):
        print
        print "Kubernetes client configuration.."
        self._cfg_dir = os.path.join(self._cfg_dir_base, name)
        self._cfg_file = os.path.join(self._cfg_dir, 'config')

        if not os.path.exists(self._cfg_file):
            raise Exception("Cannot find configuration for cluster %s at %s"%(name, self._cfg_file))

        config.load_kube_config(config_file=self._cfg_file)
        c = Configuration()
        c.assert_hostname = False
        Configuration.set_default(c)
        print("Kubernetes cluster: %s" % name)
        print("Kubernetes master: %s" % c.host)
        print("Kubernetes config used: %s" % self._cfg_file)
        print("Kubernetes cluster ssl ca: %s" % c.ssl_ca_cert)
        print("Kubernetes cluster cert: %s" % c.cert_file)
        print("Kubernetes cluster key: %s" % c.key_file)

        return c

    def find_cluster_config(self):
        cluster_configs = filter(lambda x: os.path.isdir(os.path.join(self._cfg_dir_base, x)), os.listdir(self._cfg_dir_base))

        if not cluster_configs:
            raise Exception("Cannot find any configuration in %s" % self._cfg_dir_base)

        # Select config
        menu = qprompt.Menu()
        for i in range(0, len(cluster_configs)):
            cluster_name = os.path.os.path.basename(cluster_configs[i])
            menu.add(str(i), cluster_name)
        print
        choice = menu.show(header="Select cluster configuration [found in %s]" % self._cfg_dir_base)
        selected_cluster_name = cluster_configs[int(choice)]

        return selected_cluster_name
