from . import utils
from kubernetes import client
from kubernetes.stream import stream
import yaml
import time
from tempfile import TemporaryFile
import os
import tarfile
import urllib

class KubeClusterInfo(object):
    def __init__(self, name, api_address, ca, key, cert):
        self.name = name
        self.api_address = api_address
        self.ca = ca # Full content of certificate
        self.key = key # Full content of certificate
        self.cert = cert # Full content of certificate

    def __repr__(self):
        return "<Name: %s>, <IP: %s>, <CA: %s>, <KEY: %s>, <CERT: %s>" % (self.name, self.api_address, self.ca, self.key, self.cert)

class SparkKubeBase():

    __rss_deploy_template = """
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: spark-resource-staging-server
spec:
  replicas: 1
  template:
    metadata:
      labels:
        resource-staging-server-instance: default
    spec:
      volumes:
        - name: resource-staging-server-properties
          configMap:
            name: spark-config
        - name: client-certs
          secret:
            secretName: spark-secrets
      containers:
        - name: spark-resource-staging-server
          image: gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-resource-staging-server
          imagePullPolicy: Always
          resources:
            requests:
              cpu: 500m
              memory: 512Mi
            limits:
              cpu: 1000m
              memory: 1Gi
          volumeMounts:
            - name: resource-staging-server-properties
              mountPath: '/etc/spark-resource-staging-server'
            - name: client-certs
              mountPath: '/spark-secrets'
          args:
            - '/etc/spark-resource-staging-server/rss-defaults.conf'
"""

    __edge_deploy_template = """
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: spark-edge
spec:
  replicas: 1
  template:
    metadata:
      labels:
        spark-edge-instance: default
    spec:
      volumes:
        - name: spark-config-properties
          configMap:
            name: spark-config
        - name: client-certs
          secret:
            secretName: spark-secrets
      containers:
        - name: spark-edge
          image: gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-edge
          imagePullPolicy: Always
          resources:
            requests:
              cpu: 1000m
              memory: 2Gi
            limits:
              cpu: 1000m
              memory: 2Gi
          env:
            - name: KRB5CCNAME
              value: {krbcc_spark}
            - name: HADOOP_CONF_DIR
              value: {hadoop_conf_dir}
            - name: SPARK_APP_JAR_PATH
              value: {spark_app_jar_path}
          volumeMounts:
            - name: spark-config-properties
              mountPath: '/opt/spark/conf'
            - name: client-certs
              mountPath: '/spark-secrets'
"""

    __spark_config_template = """
apiVersion: v1
kind: ConfigMap
metadata:
  name: spark-config
data:
  rss-defaults.conf: |
    spark.kubernetes.resourceStagingServer.port=10000
    spark.ssl.kubernetes.resourceStagingServer.enabled=false
    spark.kubernetes.authenticate.resourceStagingServer.useServiceAccountCredentials=false
    spark.kubernetes.authenticate.resourceStagingServer.caCertFile=/spark-secrets/client-ca
    spark.kubernetes.authenticate.resourceStagingServer.clientKeyFile=/spark-secrets/client-key
    spark.kubernetes.authenticate.resourceStagingServer.clientCertFile=/spark-secrets/client-cert
  spark-defaults.conf: |
    spark.submit.deployMode=cluster
    spark.kubernetes.docker.image.pullPolicy=Always
    spark.kubernetes.authenticate.submission.caCertFile=/spark-secrets/client-ca
    spark.kubernetes.authenticate.submission.clientKeyFile=/spark-secrets/client-key
    spark.kubernetes.authenticate.submission.clientCertFile=/spark-secrets/client-cert
    spark.kubernetes.authenticate.driver.caCertFile=/spark-secrets/client-ca
    spark.kubernetes.authenticate.driver.clientKeyFile=/spark-secrets/client-key
    spark.kubernetes.authenticate.driver.clientCertFile=/spark-secrets/client-cert
    spark.kubernetes.driver.docker.image=gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-driver
    spark.kubernetes.executor.docker.image=gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-executor
    spark.kubernetes.initcontainer.docker.image=gitlab-registry.cern.ch/db/spark-service/docker-registry:spark-init
"""

    __spark_secrets_template = """
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: spark-secrets
data:
  # Populate the following files with etcd TLS configuration if desired, but leave blank if
  # not using TLS for etcd.
  # This self-hosted install expects three files with the following names.  The values
  # should be base64 encoded strings of the entire contents of each file.
  client-key: {key}
  client-cert: {cert}
  client-ca: {ca}
"""

    __rss_service_template = """
apiVersion: v1
kind: Service
metadata:
  name: spark-resource-staging-service
spec:
  type: NodePort
  selector:
    resource-staging-server-instance: default
  ports:
    - protocol: TCP
      port: 10000
      targetPort: 10000
      nodePort: 31000
"""

    def __init__(self, c):
        self.configuration = c
        self.k8s_client = client.ApiClient(configuration=self.configuration)

    def __get_edge_path(self, name):
        return "/opt/spark/work-dir/%s" % name

    def __get_edge_app_jar_path(self):
        return self.__get_edge_path("spark-job.jar")

    def __get_edge_krb5_file(self):
        return self.__get_edge_path("krbcc_spark")

    def __get_edge_hadoop_conf_dir(self):
        return self.__get_edge_path("hadoop-conf-dir")


    def __contruct_spark_submit_cmd(self, conf_args, files_arg, remote_jar_path, app_args, namespace, driver_pod_name, master_ip, rss_ip):
        exec_command = [
            "/opt/spark/bin/spark-submit",
            "--kubernetes-namespace", namespace,
            "--conf", "spark.kubernetes.driver.pod.name=%s" % driver_pod_name,
            "--conf", "spark.kubernetes.resourceStagingServer.uri=http://%s:31000" % rss_ip,
            "--master", "k8s://%s" % master_ip,
        ]
        exec_command.extend(conf_args)
        exec_command.extend(files_arg)
        exec_command.append(remote_jar_path)
        exec_command.extend(app_args)
        return exec_command

    def __exec_cmd_spark_edge(self, exec_command):
        api = client.CoreV1Api(self.k8s_client)
        edge_pod = self._get_pod("spark-edge-instance=default")
        resp = stream(api.connect_get_namespaced_pod_exec, edge_pod.metadata.name, 'default',
                      command=exec_command,
                      stderr=True, stdin=True,
                      stdout=True, tty=False,
                      _preload_content=False)

        return resp

    def __exec_submit_tar_buffer(self, tar_buffer):
        exec_command = ['tar', 'xvf', '-', '-C', '/']

        resp = self.__exec_cmd_spark_edge(exec_command)

        commands = [tar_buffer]

        has_error = False
        while resp.is_open():
            resp.update(timeout=1)
            if resp.peek_stderr():
                has_error = True
                print(resp.read_stderr())
            if commands:
                c = commands.pop(0)
                resp.write_stdin(c)
            else:
                break

        resp.close()
        if has_error:
            raise Exception("Submitting files to spark edge pod failed!")

    def _clean(self):
        print "Cleaning spark kubernetes dependencies.."
        api_core = client.CoreV1Api(self.k8s_client)
        api_apps = client.ExtensionsV1beta1Api(self.k8s_client)

        print "Deleting deployment.."
        try:
            # Ensure delete is issued
            resp = api_apps.delete_namespaced_deployment(name="spark-resource-staging-server",
                                                         body=client.V1DeleteOptions(propagation_policy="Foreground", grace_period_seconds=10),
                                                         namespace="default")
        except Exception as e:
            pass

        try:
            # Ensure delete is issued
            resp = api_apps.delete_namespaced_deployment(name="spark-edge",
                                                         body=client.V1DeleteOptions(propagation_policy="Foreground", grace_period_seconds=10),
                                                         namespace="default")
        except Exception:
            pass

        print "Ensure main components are no longer running.."
        while 1:
            try:
                # Ensure is not existing
                resp = api_apps.read_namespaced_deployment(name="spark-edge", namespace="default")
                time.sleep(5)
            except Exception as e:
                break

        while 1:
            try:
                # Ensure is not existing
                resp = api_apps.read_namespaced_deployment(name="spark-resource-staging-server", namespace="default")
                time.sleep(5)
            except Exception as e:
                break

        print "Ensure dependencies are removed.."
        try:
            # Ensure delete is issued
            api_core.delete_namespaced_service(name="spark-resource-staging-service", namespace="default")
            time.sleep(5)
        except Exception as e:
            print e.message
            pass

        try:
            api_core.delete_namespaced_config_map(name="spark-config", body=client.V1DeleteOptions(), namespace="default")
        except Exception:
            pass

        try:
            api_core.delete_namespaced_secret(name="spark-secrets", body=client.V1DeleteOptions(), namespace="default")
        except Exception:
            pass

    def _create_deployments(self):
        print "Creating spark kubernetes deployment.."
        rss_dep = yaml.load(self.__rss_deploy_template)
        edge_dep = yaml.load(self.__edge_deploy_template.format(
            krbcc_spark=self.__get_edge_krb5_file(),
            hadoop_conf_dir=self.__get_edge_hadoop_conf_dir(),
            spark_app_jar_path=self.__get_edge_app_jar_path()
        ))

        api = client.AppsV1beta1Api(self.k8s_client)

        resp = api.create_namespaced_deployment(body=rss_dep, namespace="default")
        resp = api.create_namespaced_deployment(body=edge_dep, namespace="default")

    def _create_service(self):
        print "Creating spark kubernetes service.."
        dep = yaml.load(self.__rss_service_template)
        api = client.CoreV1Api(self.k8s_client)
        resp = api.create_namespaced_service(body=dep, namespace="default")

    def _create_config(self):
        print "Creating spark kubernetes config.."
        dep = yaml.load(self.__spark_config_template)
        api = client.CoreV1Api(self.k8s_client)
        resp = api.create_namespaced_config_map(body=dep, namespace="default")

    def _create_secret(self):
        print "Creating spark kubernetes secret.."
        dep = yaml.load(self.__spark_secrets_template.format(
            key=utils.read_cert(self.configuration.key_file),
            cert=utils.read_cert(self.configuration.cert_file),
            ca=utils.read_cert(self.configuration.ssl_ca_cert)
        ))
        api = client.CoreV1Api(self.k8s_client)
        resp = api.create_namespaced_secret(body=dep, namespace="default")

    def _get_pod(self, label):
        api_core = client.CoreV1Api(self.k8s_client)
        ret = api_core.list_pod_for_all_namespaces(watch=False, label_selector=label)

        pods = []
        for i in ret.items:
            if i.status.phase == 'Running':
                pods.append(i)

        if len(pods) != 1:
            raise Exception("Spark on Kubernetes supports only 1 of %s running [got %s]"
                            % (label, len(pods)))

        return pods[0]

    def _check_pod(self, label):
        pod = self._get_pod(label)
        print
        print "%s\t%s\t%s" %(pod.metadata.name, pod.metadata.namespace, pod.status.phase)

        return pod

    def _get_logs(self, pod_name, follow=False):
        api_logs = client.CoreV1Api(self.k8s_client)

        # In case of failed containers it is worth to see previous (if there are any)
        print
        if follow is False:
            try:
                api_response = api_logs.read_namespaced_pod_log(pod_name, "default", pretty='true', previous=True)
            except Exception:
                api_response = api_logs.read_namespaced_pod_log(pod_name, "default", pretty='true')
            print(api_response)
        else:
            api_response = api_logs.read_namespaced_pod_log(pod_name, "default", pretty='true')
            print(api_response)
            for line in api_logs.read_namespaced_pod_log(
                    pod_name,
                    "default",
                    follow=True,
                    _preload_content=False):
                print(line.rstrip())

    def _get_nodes(self, label_selector=None):
        api_instance = client.CoreV1Api(self.k8s_client)
        if label_selector:
            nodes = api_instance.list_node(pretty='true', label_selector=label_selector)
        else:
            nodes = api_instance.list_node(pretty='true')

        node_list = []
        if nodes.items:
            node_list = nodes.items
        return node_list

    def _exec_submit_app_deps(self, job_jar_path, job_file_paths, job_file_krb5_option=None, job_hdfs_configs_option=None):
        submitted_app_path = None
        submitted_krb5_path = None
        submitted_hdfs_config_paths = []

        with TemporaryFile() as buffer_file:
            # Prepare files to be send over to spark-submit container
            with tarfile.open(fileobj=buffer_file, mode='w:') as tar:
                # Add app jar if it not url
                if '//' not in job_jar_path:
                    submitted_app_path = self.__get_edge_app_jar_path()
                    tar.add(job_jar_path, arcname=submitted_app_path)

                # Add app dependencies (--files)
                submitted_file_paths = []
                for job_file_path in job_file_paths:
                    filename = os.path.basename(job_file_path)
                    submitted_file_path = self.__get_edge_path(filename)
                    tar.add(job_file_path, arcname=submitted_file_path)
                    submitted_file_paths.append(submitted_file_path)

                # Add kerberos ticket if required
                if job_file_krb5_option:
                    submitted_krb5_path = self.__get_edge_krb5_file()
                    tar.add(job_file_krb5_option, arcname=submitted_krb5_path)

                # Add hdfs config if needed
                if job_hdfs_configs_option:
                    for hdfs_config_file_url in job_hdfs_configs_option:
                        handle = urllib.urlopen(hdfs_config_file_url)
                        if handle.code > 300:
                            raise Exception("Cannot find resource under url %s" % hdfs_config_file_url)
                        tmp_hdfs_config_file_path = urllib.urlretrieve(hdfs_config_file_url, filename=None)[0]
                        filename = os.path.basename(hdfs_config_file_url)
                        submitted_hdfs_config_path = os.path.join(self.__get_edge_hadoop_conf_dir(), filename)
                        tar.add(tmp_hdfs_config_file_path, arcname=submitted_hdfs_config_path)
                        submitted_hdfs_config_paths.append(submitted_hdfs_config_path)
            buffer_file.seek(0)

            # After tar with files to be submitted is created, send to spark-submit container
            self.__exec_submit_tar_buffer(buffer_file.read())

        if submitted_app_path:
            print submitted_app_path
        for submitted_file_path in submitted_file_paths:
            print submitted_file_path
        for submitted_hdfs_config_path in submitted_hdfs_config_paths:
            print submitted_hdfs_config_path
        if submitted_krb5_path:
            print submitted_krb5_path

        return submitted_app_path, submitted_file_paths, submitted_krb5_path, submitted_hdfs_config_paths

    def _exec_spark_submit(self, conf_args, files_arg, remote_jar_path, jar_args, driver_pod_name):
        # Get and check that resource staging is fine (pod)
        rss_pod = self._get_pod(label="resource-staging-server-instance=default")

        # Construct spark-submit command to be executed (from spark-submit-base)
        full_cmd_to_execute = self.__contruct_spark_submit_cmd(
            conf_args=conf_args,
            files_arg=files_arg,
            remote_jar_path=remote_jar_path,
            app_args=jar_args,
            namespace="default",
            driver_pod_name=driver_pod_name,
            master_ip=self.configuration.host,
            rss_ip=rss_pod.status.host_ip
        )

        # call exec command on spark-edge-instance
        resp = self.__exec_cmd_spark_edge(
            exec_command=full_cmd_to_execute
        )
        print(' '.join(full_cmd_to_execute))

        # Fetch logs from response of edge and from driver
        has_error = False
        is_driver_initialized = False
        while resp.is_open():
            try:
                if is_driver_initialized:
                    # First try to fetch logs checking if driver already responds
                    self._get_logs(pod_name=driver_pod_name, follow=False)
                    is_driver_initialized = True
                else:
                    # Attach to the driver and stream the logs continuously
                    # till the driver finishes, and then break the loop
                    self._get_logs(pod_name=driver_pod_name, follow=True)
                    break
            except Exception:
                pass

            time.sleep(1)
            resp.update()
            if resp.peek_stdout():
                print(resp.read_stdout())
            if resp.peek_stderr():
                has_error = True
                print(resp.read_stderr())

        resp.close()
        if has_error:
            raise Exception("Exec spark-submit failed")

    def get_labeled_nodes(self, label_selector):
        raise Exception("Interface not implemented")

    def create_spark_deps(self):
        raise Exception("Interface not implemented")

    def check_spark_deps(self):
        raise Exception("Interface not implemented")

    def exec_spark_submit(self, conf_args_list, job_file_paths, source_jar_path, jar_args):
        raise Exception("Interface not implemented")

class SparkKubeApi(SparkKubeBase):

    def get_labeled_nodes(self, label_selector):
        nodes = self._get_nodes(label_selector=label_selector)

        print
        print "Found %s nodes in the cluster for label selector [%s]." % (len(nodes), label_selector)

        return nodes

    def create_spark_deps(self):
        self._clean()

        self._create_secret()
        self._create_config()
        self._create_service()
        self._create_deployments()
        time.sleep(60)

    def check_spark_deps(self):
        edge_pod = self._check_pod("spark-edge-instance=default")
        rss_pod = self._check_pod("resource-staging-server-instance=default")

        self._get_logs(rss_pod.metadata.name)
        self._get_logs(edge_pod.metadata.name)

    def exec_spark_submit(self, conf_args_list, job_file_paths, source_jar_path, jar_args, hdfs_auth=None, eos_auth=None, hdfs_config_files=None):
        app_name = "spark-app-driver-%s" % utils.get_timestamp()

        if hdfs_auth:
            krb5_ticket = hdfs_auth
        elif eos_auth:
            krb5_ticket = eos_auth
        else:
            krb5_ticket = None

        print
        print "Submitting spark job jar and files to spark-submit pod.."
        # Submit files (--files) and kerberos ticket (optional) into spark working directory
        app_location, file_locations, krb5_location, hdfs_conf_locations = self._exec_submit_app_deps(
            job_jar_path=source_jar_path,
            job_file_paths=job_file_paths,
            job_file_krb5_option=krb5_ticket,
            job_hdfs_configs_option=hdfs_config_files
        )

        if not app_location:
            app_location = source_jar_path
        if eos_auth:
            # EOS requires ticket to be send to driver/executors
            file_locations.append(krb5_location)
            conf_args_list.append("--conf")
            conf_args_list.append("spark.kubernetes.driverEnv.KRB5CCNAME=%s" % krb5_location)
            conf_args_list.append("--conf")
            conf_args_list.append("spark.executorEnv.KRB5CCNAME=%s" % krb5_location)
        elif hdfs_auth:
            # HDFS requires to generate kerberos token from the client
            conf_args_list.append("--conf")
            conf_args_list.append("spark.kubernetes.kerberos.enabled=true")

        files_arg = []
        if file_locations:
            files_arg.append("--files")
            files_arg.append(','.join(file_locations))

        print
        print "Executing spark-submit.."
        self._exec_spark_submit(
            conf_args=conf_args_list,
            files_arg=files_arg,
            remote_jar_path=app_location,
            jar_args=jar_args,
            driver_pod_name=app_name
        )
