from . import base_command
from . import cluster
from . import kube_config_client

class KubeFetch():
    @staticmethod
    def save_config(kube_cluster_info):
        if not isinstance(kube_cluster_info, cluster.KubeClusterInfo):
            raise Exception('Command received as argument wrong class type')

        print
        print "Kubernetes client configuration.."

        # Kubernetes has been initialized on the cluster, use configuration and
        # store permanently on the client
        client = kube_config_client.KubeConfigClient()
        client.save_cluster_info(kube_cluster_info)

        print
        print "Finished client configuration"
        return True

class KubeFetchCommand(base_command.BaseCommand):
    def run(self, args, additional):
        """
        :param args array: arguments passed to the command
        :return: bool: whether command was successful or not
        """

        if args.openstack_config:
            return OpenstackKubeFetchConfigCommand().run()
        elif args.openstack_cluster_info:
            return OpenstackKubeFetchClusterInfoCommand().run()
        else:
            return False


class OpenstackKubeFetchClusterInfoCommand():

    def run(self):
        """
        :param skip_cluster bool: indicates whether the cluster should be created, if false only cluster configuration will be fetched
        :return: KubeCluster
        """
        from . import openstack_client
        import json

        # Initialize client
        client = openstack_client.CernOpentackClient()

        # Select a project in openstack
        selected_project = client.get_project()

        # Select kube cluster name
        name = client.select_cluster(selected_project.id)

        # Get created cluster config
        cluster_info_json = client.get_cluster_info(selected_project.id, name).to_dict()

        print json.dumps(cluster_info_json, indent=4, sort_keys=True)
        return True

class OpenstackKubeFetchConfigCommand():

    def run(self):
        """
        :param skip_cluster bool: indicates whether the cluster should be created, if false only cluster configuration will be fetched
        :return: KubeCluster
        """
        from . import openstack_client
        kube_fetch = KubeFetch()

        # Initialize client
        client = openstack_client.CernOpentackClient()

        # Select a project in openstack
        selected_project = client.get_project()

        # Select kube cluster name
        name = client.select_cluster(selected_project.id)

        # Get created cluster config
        kube_cluster_info = client.get_cluster(selected_project.id, name)

        # Create/refresh local config
        kube_fetch.save_config(kube_cluster_info)

        return True
