
__title__ = 'cern-spark-service'
__description__ = 'CERN Spark Service Tools'
__url__ = 'https://gitlab.cern.ch/db/spark-service/spark-service-tools'
__version__ = '0.9.1'
__build__ = 0x021804
__author__ = 'CERN-IT-DB-SAS'
__author_email__ = 'piotr.mrowczynski@cern.ch'
__license__ = 'Apache 2.0'
__copyright__ = 'Copyright 2018 CERN'
__python_requires__ = '>=2.7.5, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*'
__python_min_version__ = '2.7.5'
