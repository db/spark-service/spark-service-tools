# CERN Spark Service Tools

**This repository holds all tools and instructions for running spark on kubernetes**


##### Documentation
For package documentation, please go to it's PyPi repository [https://pypi.org/project/cern-spark-service](https://pypi.org/project/cern-spark-service) 
and uses **README.rst** from this repository

##### Examples
Examples of package usage can be found here [https://gitlab.cern.ch/db/spark-service/spark-service-examples](https://gitlab.cern.ch/db/spark-service/spark-service-examples) 
